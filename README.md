### BXC_VideoAnalyzer_v3
* 作者：北小菜 
* 个人网站：http://www.any12345.com
* 邮箱：bilibili_bxc@126.com
* QQ：1402990689
* 微信：bilibili_bxc
* github安装包下载地址：https://github.com/any12345com/BXC_VideoAnalyzer_v3
* gitee安装包地址：https://gitee.com/Vanishi/BXC_VideoAnalyzer_v3

#### 介绍

视频行为分析系统V3版本编译安装包，下载后可以直接运行，本次V3版本更新的内容非常多，相比于过去的两个版本功能更加完善成熟，V3版本可以作为产品直接使用。
（当前仅提供Windows版编译安装包，其他平台需要自行编译源码）

<img width="720" alt="控制面板" src="https://gitee.com/Vanishi/BXC_VideoAnalyzer_v3/raw/master/data/1.png">
<img width="720" alt="视频流管理" src="https://gitee.com/Vanishi/BXC_VideoAnalyzer_v3/raw/master/data/2.png">
<img width="720" alt="报警视频管理" src="https://gitee.com/Vanishi/BXC_VideoAnalyzer_v3/raw/master/data/3.png">
<img width="720" alt="算法管理" src="https://gitee.com/Vanishi/BXC_VideoAnalyzer_v3/raw/master/data/4.png">
<img width="720" alt="布控管理" src="https://gitee.com/Vanishi/BXC_VideoAnalyzer_v3/raw/master/data/5.png">
<img width="720" alt="编辑布控" src="https://gitee.com/Vanishi/BXC_VideoAnalyzer_v3/raw/master/data/6.png">
<img width="720" alt="播放算法视频流" src="https://gitee.com/Vanishi/BXC_VideoAnalyzer_v3/raw/master/data/7.png">


##### V3主要更新功能点
* 1 基础算法模型升级，yolov5升级至yolov8
* 2 基础算法模型推理继续采用openvino，区别在于 V1，V2版本是基于python版openvino调用，V3版本则是基于c++调用，c++版可以大幅度减少性能损耗和程序交互复杂度，极大提升了视频分析的计算频率。
* 3 布控算法模块，周界入侵算法功能完善，支持绘制算法识别区域，支持监测周界入侵的分类，比如支持监测人，狗，猫等等分类。
* 4 分析器模块和算法模块合并，在V1和V2版本中两个模块各自独立，通过api接口进行交互，V3版本中两个模块合并，通信也不需要再借助api而是直接调用，效率大幅度提升。
* 5 流媒体模块跟随开源代码的更新而更新，同时二次开发中去除不需要的流媒体协议
* 6 后台管理模块，新增了合成报警视频的管理功能，可以对报警视频进行增删改查，布控中的视频产生的报警视频会出现在这个功能中。同时在布控过程中，支持绘制布控周界区域和选择监测分类。

* 视频行为分析系统V1版本视频介绍地址：[https://www.bilibili.com/video/BV1dG4y1k77o](https://www.bilibili.com/video/BV1dG4y1k77o)
* 视频行为分析系统V2版本视频介绍地址：[https://www.bilibili.com/video/BV1CG411f7ak](https://www.bilibili.com/video/BV1CG411f7ak)


### 启动程序
* （编译安装包）直接运行 VideoAnalyzer.exe 即启动整个项目，启动项目前可以参考下面的配置说明
* 其他平台需要自行编译源码运行



### 配置说明
~~~
//config.json
{
  "version": "3",       //程序版本号
  "host": "127.0.0.1",  //部署机器IP地址（可以使用127.0.0.1,推荐使用ipconfig获取本机局域网IP地址）
  "adminPort": 9001,    //后台管理服务器端口
  "analyzerPort": 9002, //分析器服务端口
  "mediaHttpPort": 9003,//流媒体服务器端口
  "mediaRtspPort": 554, //流媒体服务器RTSP协议端口
  "mediaSecret": "aqxY9ps21fyhyKNRyYpGvJCTp1JBeGOM", //流媒体服务器安全码
  "rootDir": "www",     			     //报警产生的视频或图片存储位置
  "videoFileNameFormat": "%Y%m%d%H%M%S", //报警产生的视频名称格式
  "recordLogInterval": 30,               //主程序正常启动后记录日志的间隔时间，单位秒
  "workerConcurrency": 20,               //最大布控视频流数量
  "supportHardwareVideoDecode": false,   //是否支持硬件解码（建议关闭硬件解码，将硬件资源留给算法）
  "supportHardwareVideoEncode": false,   //是否支持硬件编码（建议关闭硬件编码，将硬件资源留给算法）
  "algorithmWeight": "models\\yolov8n.xml", //算法模型权重数据
  "algorithmDevice": "GPU",                 //算法模型运行设备，CPU or GPU
  "algorithmWeightConcurrency": 1           //分析器启动时，加载算法模型的并发数

}

~~~

### ffmpeg命令行推流

~~~

//将本地文件推流至VideoAnalyzer（该命令行未经优化，延迟较大）
ffmpeg -re -stream_loop -1  -i test.mp4  -rtsp_transport tcp -c copy -f rtsp rtsp://127.0.0.1:554/live/test

//将摄像头视频流推流至VideoAnalyzer（该命令行已优化，但仍然存在延迟，如果想要彻底解决推流延迟，可以参考我的视频：https://space.bilibili.com/487906612）
ffmpeg  -rtsp_transport tcp -i url -fflags nobuffer -max_delay 1 -threads 5  -profile:v high  -preset superfast -tune zerolatency  -an -c:v h264 -crf 25 -s 1280*720   -f rtsp -bf 0  -g 5  -rtsp_transport tcp rtsp://127.0.0.1:554/live/camera

// 备注
根目录下data文件夹中，我提供了一个test.mp4，大家可以测试，模拟视频流

~~~

### 有关ffmpeg推流的几点补充说明

* 通过ffmpeg命令行实现的推流功能，延迟总是存在的，且无法解决。但基于ffmpeg开发库却可以彻底解决延迟推流的问题，可以参考我的视频：https://space.bilibili.com/487906612
